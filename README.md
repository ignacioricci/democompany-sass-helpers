# Sass Helpers Demo

## Pre-requisites

* Node.js
* NPM

## Installation

```
npm install
```

## Usage

```
npm run compile
npm run watch (listens to changes)
```

To use CDN assets, use:

```
npm run compile production
npm run watch production
```